﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections;
using System.Collections.Generic;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class AllCurrentTimeController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            List<CurrentTimeQuery> returnVal = new List<CurrentTimeQuery>();
            using (var db = new ClockworkContext())
            {
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    returnVal.Add(CurrentTimeQuery);
                }
            }
            returnVal = returnVal.GetRange((returnVal.Count - 10), 10);//only take last 10
            return Ok(returnVal);
        }
    }
}