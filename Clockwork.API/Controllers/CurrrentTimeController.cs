﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections;
using System.Collections.Generic;
//using System.Web.Http;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    //[RoutePrefix("api/currenttime")]
    public class CurrentTimeController : Controller
    {
        //[System.Web.Http.Route("get")]
        [HttpGet]
        public IActionResult Get(string clientTimeZone)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, clientTimeZone);
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
            var timeZone = clientTimeZone;

            var returnVal = new CurrentTimeQuery()
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                ClientTimeZone = timeZone
            };

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
                Console.WriteLine($"{count} records saved to database");

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine($" - {CurrentTimeQuery.UTCTime}");
                }
            }

            return Ok(returnVal);
        }
    }
}
