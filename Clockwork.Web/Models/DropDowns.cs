﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clockwork.Web.Models
{
    public class DropDowns
    {
        private List<SelectListItem> timeZoneList = new List<SelectListItem>();
        public List<SelectListItem> TimeZoneList
        {
            get
            {
                foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                {
                    SelectListItem temp = new SelectListItem { Value = z.Id, Text = z.DisplayName };
                    timeZoneList.Add(temp);
                }
                return timeZoneList;
            }
        }
    }
}