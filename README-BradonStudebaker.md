# Read Me #
Welcome to my solution for the AWH interview exercise!
This project was a lot of fun, and I learned a great deal. Having never really worked in
.NET Core or Entity framework, I supplemented this exercise with a great deal
of podcast listening and online forum searching. 

### Problem Solving ###
As soon as a I opened this project, I realized it would have an intersting learning
curve do to it combining multiple elements I hadn't fully worked with.
Before starting I read a great deal of documentation and continued to do so throughout.
Some of the most challening problems I had to face were adding a new property to the database
and deciding how to unify the timezone problem. After some deliberation, I decided due to the 
conversions I needed to perform that using the .NET ``TimeZoneInfo`` class would be the best 
way of dealing with things across the board. Creating my own Migrations for the client time zone
was initially a challenge due to having not done it, but it proved to be a simple to implement 
process after some reading. Thanks to the Trello Board, most of the planning was done for me which
left me only having to create the implementation, which cut down on the pre-code phase significantly.

### What I'd Change ###
If I were to do this again, I would have been committing early and often. I had a bit of a brain lapse!
This is one important strategy I missed on.
If I were to push the front end further, I would aesthetically ramp up everything across the board from the 
vanilla design that is present.
If I were to push the backend further, I would look more into using some of the newer technologies available to 
Core 2.1 such as API Routing or an Http client factory to streamline microservices. I would also look into more
ways to write tests, possibly by delegating some of the logic
to a DAL/O(s).

### Set Up ###
* Install Visual Studio 2017. Note that you must you the 2017 edition.
* Clone the repo
* Check CORS access
* Assure packages are restored that are necessary using NuGet
* Run Migrations
* Assure IP settings for API calls align with launch settings

### Built With ###
* Microsoft Visual Studio: for core development
* NuGet CLI: for internal dependencies and packages 
* Postman: for testing http requests
* Linqpad with IQ Plugin: for testing information in .db file
* Bootstrap: for a rapidly built responsive design 

### Authors ###
* AWH
* Bradon Studebaker
